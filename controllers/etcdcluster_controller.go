/*
Copyright 2022 Tony Yip.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"strings"

	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmm "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	apps "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	etcdv1alpha1 "gitlab.com/oss-cloud/kaas/etcd-operator/api/v1alpha1"
)

// EtcdClusterReconciler reconciles a EtcdCluster object
type EtcdClusterReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=etcd.kaas.oss-cloud.gitlab.io,resources=etcdclusters,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=etcd.kaas.oss-cloud.gitlab.io,resources=etcdclusters/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=etcd.kaas.oss-cloud.gitlab.io,resources=etcdclusters/finalizers,verbs=update
//+kubebuilder:rbac:groups=apps,resources=statefulsets,verbs=get;create;update;delete
//+kubebuilder:rbac:groups=cert-manager.io,resources=certificates,verbs=get;create;update;delete
//+kubebuilder:rbac:groups=core,resources=services,verbs=get;create;update;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// Modify the Reconcile function to compare the state specified by
// the EtcdCluster object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.12.1/pkg/reconcile
func (r *EtcdClusterReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	cluster := &etcdv1alpha1.EtcdCluster{}
	if err := r.Get(ctx, req.NamespacedName, cluster); err != nil {
		if errors.IsNotFound(err) {
			logger.Info("resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}

		// Error reading the object - requeue the request.
		logger.Error(err, "Failed to get resource")
		return ctrl.Result{}, err
	}

	for _, ensureFunc := range []func(context.Context, *etcdv1alpha1.EtcdCluster) error{
		r.ensureService,
		r.ensureCertificate,
		r.ensureStatefulSet,
	} {
		if err := ensureFunc(ctx, cluster); err != nil {
			logger.Error(err, "fail reconcile")
			return ctrl.Result{}, err
		}
	}

	cluster.Status.Stage = etcdv1alpha1.StageReady
	if cluster.Status.Conditions == nil {
		cluster.Status.Conditions = make([]etcdv1alpha1.EtcdClusterCondition, 0, 1)
	}
	cluster.Status.Conditions = append(cluster.Status.Conditions, etcdv1alpha1.EtcdClusterCondition{
		Stage:          etcdv1alpha1.StageReady,
		Message:        "Finish reconcile",
		TransitionTime: metav1.Now(),
	})
	if err := r.Status().Update(ctx, cluster); err != nil {
		logger.Error(err, "failed to update status")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *EtcdClusterReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&etcdv1alpha1.EtcdCluster{}).
		Owns(&apps.StatefulSet{}).
		Owns(&v1.Service{}).
		Owns(&certmanager.Certificate{}).
		Complete(r)
}

func (r *EtcdClusterReconciler) resourceName(cluster *etcdv1alpha1.EtcdCluster) string {
	return fmt.Sprintf("%s-etcd", cluster.GetName())
}

func (r *EtcdClusterReconciler) ensureService(ctx context.Context, cluster *etcdv1alpha1.EtcdCluster) error {
	logger := log.FromContext(ctx)

	svc := &v1.Service{}
	err := r.Get(ctx, client.ObjectKey{Namespace: cluster.GetNamespace(), Name: r.resourceName(cluster)}, svc)

	if err != nil && !errors.IsNotFound(err) {
		// Error reading the object - requeue the request.
		logger.Error(err, "Failed to get resource")
		return err
	}

	create := false

	if err != nil {
		create = true
		err = nil

		svc.Name = r.resourceName(cluster)
		svc.Namespace = cluster.GetNamespace()
	}

	if svc.Labels == nil {
		svc.Labels = make(map[string]string)
	}
	svc.Labels["app.kubernetes.io/name"] = "etcd"
	svc.Labels["app.kubernetes.io/instance"] = cluster.GetName()
	svc.Labels["app.kubernetes.io/managed-by"] = "etcd-operator"

	svc.Spec.PublishNotReadyAddresses = true
	svc.Spec.Type = v1.ServiceTypeClusterIP
	svc.Spec.ClusterIP = "None"
	svc.Spec.Selector = map[string]string{
		"app.kubernetes.io/name":       "etcd",
		"app.kubernetes.io/instance":   cluster.GetName(),
		"app.kubernetes.io/managed-by": "etcd-operator",
	}
	svc.Spec.Ports = []v1.ServicePort{
		{
			Name: "etcd-client-ssl",
			Port: 2379,
		},
		{
			Name: "etcd-server-ssl",
			Port: 2380,
		},
	}

	_ = ctrl.SetControllerReference(cluster, svc, r.Scheme)

	if create {
		return r.Create(ctx, svc)
	}

	return r.Update(ctx, svc)
}

func (r *EtcdClusterReconciler) ensureCertificate(ctx context.Context, cluster *etcdv1alpha1.EtcdCluster) error {
	logger := log.FromContext(ctx)

	name := r.resourceName(cluster)

	cert := &certmanager.Certificate{}
	err := r.Get(ctx, client.ObjectKey{Namespace: cluster.GetNamespace(), Name: name}, cert)

	if err != nil && !errors.IsNotFound(err) {
		// Error reading the object - requeue the request.
		logger.Error(err, "Failed to get resource")
		return err
	}

	create := false

	if err != nil {
		create = true
		err = nil

		cert.Name = name
		cert.Namespace = cluster.GetNamespace()
	}

	cert.Labels["app.kubernetes.io/name"] = "etcd"
	cert.Labels["app.kubernetes.io/instance"] = cluster.GetName()
	cert.Labels["app.kubernetes.io/managed-by"] = "etcd-operator"

	cert.Spec = certmanager.CertificateSpec{
		CommonName: name,
		DNSNames: []string{
			name,
			fmt.Sprintf("%s.%s", name, cluster.GetNamespace()),
			fmt.Sprintf("%s.%s.svc", name, cluster.GetNamespace()),
			fmt.Sprintf("%s.%s.svc.cluster.local", name, cluster.GetNamespace()),
			fmt.Sprintf("*.%s", name),
			fmt.Sprintf("*.%s.%s", name, cluster.GetNamespace()),
			fmt.Sprintf("*.%s.%s.svc", name, cluster.GetNamespace()),
			fmt.Sprintf("*.%s.%s.svc.cluster.local", name, cluster.GetNamespace()),
		},
		Usages: []certmanager.KeyUsage{
			certmanager.UsageServerAuth,
			certmanager.UsageClientAuth,
		},
		SecretName: fmt.Sprintf("%s-cert", name),
		PrivateKey: &certmanager.CertificatePrivateKey{
			Algorithm: certmanager.RSAKeyAlgorithm,
			Size:      4096,
		},
		IssuerRef: cmm.ObjectReference{
			Name: cluster.Spec.IssuerRef.Name,
			Kind: cluster.Spec.IssuerRef.Kind,
		},
	}

	_ = ctrl.SetControllerReference(cluster, cert, r.Scheme)

	if create {
		return r.Create(ctx, cert)
	}

	return r.Update(ctx, cert)
}

func (r *EtcdClusterReconciler) ensureStatefulSet(ctx context.Context, cluster *etcdv1alpha1.EtcdCluster) error {
	logger := log.FromContext(ctx)

	name := r.resourceName(cluster)

	sts := &apps.StatefulSet{}
	err := r.Get(ctx, client.ObjectKey{Namespace: cluster.GetNamespace(), Name: name}, sts)

	if err != nil && !errors.IsNotFound(err) {
		// Error reading the object - requeue the request.
		logger.Error(err, "Failed to get resource")
		return err
	}

	create := false

	if err != nil {
		create = true
		err = nil

		sts.Name = name
		sts.Namespace = cluster.GetNamespace()
	}

	sts.Labels["app.kubernetes.io/name"] = "etcd"
	sts.Labels["app.kubernetes.io/instance"] = cluster.GetName()
	sts.Labels["app.kubernetes.io/managed-by"] = "etcd-operator"

	memberList := make([]string, 0, int(cluster.Spec.Size))
	for i := int(cluster.Spec.Size) - 1; i >= 0; i -= 1 {
		memberList = append(
			memberList,
			fmt.Sprintf("%s-%d=https://%s-%d.%s.%s.svc.cluster.local:2380", name, i, name, i, name, cluster.Namespace),
		)
	}

	sts.Spec = apps.StatefulSetSpec{
		ServiceName: name,
		Replicas:    &cluster.Spec.Size,
		Selector: &metav1.LabelSelector{
			MatchLabels: map[string]string{
				"app.kubernetes.io/name":       "etcd",
				"app.kubernetes.io/instance":   cluster.GetName(),
				"app.kubernetes.io/managed-by": "etcd-operator",
			},
		},
		Template: v1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: map[string]string{
					"app.kubernetes.io/name":       "etcd",
					"app.kubernetes.io/instance":   cluster.GetName(),
					"app.kubernetes.io/managed-by": "etcd-operator",
				},
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name:            "etcd",
						Image:           cluster.Spec.Image,
						ImagePullPolicy: v1.PullIfNotPresent,
						Args: []string{
							"etcd",
							"--name=$(POD_NAME)",
							"--data-dir=/var/lib/etcd",
							fmt.Sprintf("--initial-cluster=%s", strings.Join(memberList, ",")),
							fmt.Sprintf("--advertise-client-urls=https://$(POD_NAME).%s-etcd.%s.svc.cluster.local:2379", name, cluster.GetNamespace()),
							fmt.Sprintf("--initial-advertise-peer-urls=https://$(POD_NAME).%s-etcd.%s.svc.cluster.local:2380", name, cluster.GetNamespace()),
							fmt.Sprintf("--initial-cluster-token=%s", name),
							"--listen-client-urls=https://0.0.0.0:2379",
							"--listen-peer-urls=https://0.0.0.0:2380",
							"--listen-metrics-urls=http://0.0.0.0:2381",
							"--client-cert-auth=true",
							"--cert-file=/tls/tls.crt",
							"--key-file=/tls/tls.key",
							"--trusted-ca-file=/tls/ca.crt",
							"--peer-client-cert-auth=true",
							"--peer-cert-file=/tls/tls.crt",
							"--peer-key-file=/tls/tls.key",
							"--peer-trusted-ca-file=/tls/ca.crt",
							"--snapshot-count=10000",
						},
						Env: []v1.EnvVar{
							{
								Name: "POD_NAME",
								ValueFrom: &v1.EnvVarSource{
									FieldRef: &v1.ObjectFieldSelector{
										FieldPath: "metadata.name",
									},
								},
							},
						},
						Ports: []v1.ContainerPort{
							{Name: "etcd-client-ssl", ContainerPort: 2379},
							{Name: "etcd-server-ssl", ContainerPort: 2380},
							{Name: "metrics", ContainerPort: 2381},
						},
						VolumeMounts: []v1.VolumeMount{
							{Name: "data", MountPath: "/var/lib/etcd"},
							{Name: "tls", MountPath: "/tls", ReadOnly: true},
						},
					},
				},
				Volumes: []v1.Volume{
					{
						Name: "tls",
						VolumeSource: v1.VolumeSource{
							Secret: &v1.SecretVolumeSource{
								SecretName: fmt.Sprintf("%s-cert", name),
							},
						},
					},
				},
			},
		},
		VolumeClaimTemplates: []v1.PersistentVolumeClaim{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: "data",
					Labels: map[string]string{
						"app.kubernetes.io/name":       "etcd",
						"app.kubernetes.io/instance":   cluster.GetName(),
						"app.kubernetes.io/managed-by": "etcd-operator",
					},
				},
				Spec: v1.PersistentVolumeClaimSpec{
					AccessModes: []v1.PersistentVolumeAccessMode{
						v1.ReadWriteOnce,
					},
					Resources: v1.ResourceRequirements{
						Requests: map[v1.ResourceName]resource.Quantity{
							v1.ResourceStorage: resource.MustParse(cluster.Spec.StorageSize),
						},
					},
					StorageClassName: cluster.Spec.StorageClass,
				},
			},
		},
	}

	if create {
		return r.Create(ctx, sts)
	}

	return r.Update(ctx, sts)
}
