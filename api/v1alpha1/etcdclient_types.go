/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// EtcdClientSpec defines the desired state of EtcdClient
type EtcdClientSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// ClusterName specify the name of EtcdCluster
	// +kubebuilder:validation:Required
	ClusterName string `json:"clusterName"`
}

// EtcdClientStatus defines the observed state of EtcdClient
type EtcdClientStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// SecretName specify the secret of certificate
	SecretName string `json:"secretName"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// EtcdClient is the Schema for the etcdclients API
type EtcdClient struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   EtcdClientSpec   `json:"spec,omitempty"`
	Status EtcdClientStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// EtcdClientList contains a list of EtcdClient
type EtcdClientList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []EtcdClient `json:"items"`
}

func init() {
	SchemeBuilder.Register(&EtcdClient{}, &EtcdClientList{})
}
